import { Component, OnInit } from '@angular/core';
import { UserService } from './../user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  constructor(private userService: UserService) { }
  profile = {
    results: [{
      name: {
        title: '',
        first: '',
        last: '',
      },
      gender: '',
      location: {
        street: '',
        city: '',
        state: ''
      },
      email: '',
      phone: '',
      dob: '',
      picture: {
        large: ''
      }

    }]
  };


  ngOnInit() {
    this.userService.getUser().subscribe(data => this.profile = data);
  }

}
